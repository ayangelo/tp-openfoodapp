package fr.isima.ayangelo.openfoodapp;

public enum FoodClassification {
    TROP_BON("Trop bon","green"),
    BON("Bon", "light green"),
    MANGEABLE("Mangeable","yellow"),
    MOUAIS("Mouais","orange"),
    DEGUEU("Dégueu","red");

    private final String classification;
    private final String color;

    FoodClassification(String classification, String color){
        this.classification = classification;
        this.color = color;
    }

    public String getClassification() {
        return classification;
    }

    public String getColor() {
        return color;
    }

    public static FoodClassification fromScore(int score) {
        if (score > 18) {
            return FoodClassification.DEGUEU;
        }
        else if (score > 10) {
            return FoodClassification.MOUAIS;
        }
        else if (score > 2) {
            return FoodClassification.MANGEABLE;
        }
        else if (score > -1) {
            return FoodClassification.BON;
        }
        else {
            return FoodClassification.TROP_BON;
        }
    }
}

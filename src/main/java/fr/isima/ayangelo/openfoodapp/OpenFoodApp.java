package fr.isima.ayangelo.openfoodapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenFoodApp {

	public static void main(String[] args) {

		SpringApplication.run(OpenFoodApp.class, args);
	}

}

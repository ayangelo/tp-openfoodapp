package fr.isima.ayangelo.openfoodapp.controllers;

import fr.isima.ayangelo.openfoodapp.dto.Basket;
import fr.isima.ayangelo.openfoodapp.dto.NutritionalInfo;
import fr.isima.ayangelo.openfoodapp.services.NutritionalInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
public class FoodController {

    @Autowired
    private NutritionalInfoService service;

    @GetMapping("/product/{barCode}")
    public NutritionalInfo getScore(@PathVariable String barCode) {
        return service.getInfo(barCode).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/basket")
    public int getScore(@Valid @RequestBody Basket basket){
        return basket.getProductBarCodes().stream()
                .map(productId -> service.getInfo(productId))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .mapToInt(NutritionalInfo::getScore)
                .sum();
    }
}

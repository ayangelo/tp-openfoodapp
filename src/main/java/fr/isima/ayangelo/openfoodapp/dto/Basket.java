package fr.isima.ayangelo.openfoodapp.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.util.List;

public class Basket {
    @Pattern(regexp=".+@.+\\..+", message="Please provide a valid email address")
    private final String email;

    @NotEmpty
    private final List<String> productBarCodes;

    public Basket(String email, List<String> productBarCodes){
        this.email = email;
        this.productBarCodes = productBarCodes;
    }

    public String getEmail() {
        return this.email;
    }

    public List<String> getProductBarCodes() {
        return this.productBarCodes;
    }
}

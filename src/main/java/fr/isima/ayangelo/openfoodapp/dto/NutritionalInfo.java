package fr.isima.ayangelo.openfoodapp.dto;

import com.google.gson.JsonObject;
import fr.isima.ayangelo.openfoodapp.FoodClassification;
import fr.isima.ayangelo.openfoodapp.utilitaries.NutritionalScoreComputer;

public class NutritionalInfo {
    private final String barCode;
    private final String name;
    private final int score;
    private final String color;
    private final String classification;

    public NutritionalInfo(String barCode, String name, int score) {
        this.barCode = barCode;
        this.name = name;
        this.score = score;
        FoodClassification enumClass = FoodClassification.fromScore(score);
        this.classification = enumClass.getClassification();
        this.color = enumClass.getColor();
    }

    public String getBarCode() {
        return barCode;
    }

    public String getName() {
        return name;
    }

    public int getScore() {
        return score;
    }

    public String getClassification() {
        return classification;
    }

    public String getColor() {
        return color;
    }

    public static NutritionalInfo fromJson(JsonObject jsonObject) {
        JsonObject product = jsonObject.getAsJsonObject("product");
        JsonObject nutriments = product.getAsJsonObject("nutriments");

        String barCode = product.get("_id").getAsString();
        String name = product.get("product_name").getAsString();

        // some attributes may not be present in the json even if the product is valid
        // (see https://fr.openfoodfacts.org/api/v0/produit/8888002076009.json where fiber_100g is missing)

        double energy, saturatedFat, sugars, salt, fiber, proteins;

        energy = (nutriments.has("energy_100g")) ?
                nutriments.get("energy_100g").getAsDouble() : 0;
        saturatedFat = (nutriments.has("saturated-fat_100g")) ?
                nutriments.get("saturated-fat_100g").getAsDouble() : 0;
        sugars = (nutriments.has("sugars_100g")) ?
                nutriments.get("sugars_100g").getAsDouble() : 0;
        salt = (nutriments.has("salt_100g")) ?
                nutriments.get("salt_100g").getAsDouble() : 0;
        fiber = (nutriments.has("fiber_100g")) ?
                nutriments.get("fiber_100g").getAsDouble() : 0;
        proteins = (nutriments.has("proteins_100g")) ?
                nutriments.get("proteins_100g").getAsDouble() : 0;

        int score = NutritionalScoreComputer.getNutritionalScore(energy, saturatedFat, sugars, salt, fiber, proteins);

        return new NutritionalInfo(barCode, name, score);
    }
}

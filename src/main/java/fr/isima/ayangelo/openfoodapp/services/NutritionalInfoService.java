package fr.isima.ayangelo.openfoodapp.services;

import com.google.gson.JsonObject;
import fr.isima.ayangelo.openfoodapp.dto.NutritionalInfo;
import fr.isima.ayangelo.openfoodapp.utilitaries.Serializer;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

@Service
public class NutritionalInfoService {
    private final OkHttpClient client = new OkHttpClient();

    public Optional<NutritionalInfo> getInfo(String barCode){
        String url = String.format("https://fr.openfoodfacts.org/api/v0/produit/%s.json", barCode);

        Request request = new Request.Builder()
                .url(url)
                .addHeader("accept", "application/json")
                .build();

        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) throw new IOException("Unexepected request");

            JsonObject jsonProduct = Serializer.deserialize(Objects.requireNonNull(response.body()).string());
            NutritionalInfo info = NutritionalInfo.fromJson(jsonProduct);

            return Optional.of(info);
        }
        catch (Exception ignored) {
        }
        return Optional.empty();
    }
}

package fr.isima.ayangelo.openfoodapp.utilitaries;

public class NutritionalScoreComputer {

    public static int getSaltNutritionalScore(double salt) {

        return Math.min(10, Math.max(0, (int) Math.ceil(salt / 90) - 1));
    }

    public static int getSugarsNutritionalScore(double sugar) {

        if (sugar <= 27){
            return Math.min(10, Math.max(0, (int) (Math.ceil(sugar / 4.5) - 1)));
        }
        if (sugar <= 31){
            return 6;
        }
        if (sugar <= 36){
            return 7;
        }
        if (sugar <= 40){
            return 8;
        }
        if (sugar <= 45){
            return 9;
        }
        return 10;
    }

    public static int getSaturatedFatNutritionalScore(double saturatedFat) {

        return Math.min(10, Math.max(0, (int) Math.ceil(saturatedFat) -1));
    }

    public static int getEnergyNutritionalScore(double energy) {

        return Math.min(10, Math.max(0,(int) (Math.ceil(energy/335) - 1)));
    }

    private static int getNegativeNutritionalScore(double energy,  double saturatedFat, double sugar, double salt) {

        return getEnergyNutritionalScore(energy) + getSaturatedFatNutritionalScore(saturatedFat) +
               getSugarsNutritionalScore(sugar) + getSaltNutritionalScore(salt);

    }

    public static int getProteinNutritionalScore(double proteins) {

        return Math.min(5, Math.max(0, (int) (Math.ceil(proteins / 1.6) - 1)));
    }

    public static int getFiberNutritionalScore(double fiber) {

        if (fiber <= 0.9){
            return 0;
        }
        if (fiber <= 1.9){
            return 1;
        }
        if (fiber <= 2.8){
            return 2;
        }
        if (fiber <= 3.7){
            return 3;
        }
        if (fiber <= 4.7){
            return 4;
        }
        return 5;
    }

    private static int getPositiveNutritionalScore(double fiber, double proteins) {

        return getFiberNutritionalScore(fiber) + getProteinNutritionalScore(proteins);
    }

    public static int getNutritionalScore(double energy,
                                          double saturatedFat,
                                          double sugars,
                                          double salt,
                                          double fiber,
                                          double proteins) {

        return getNegativeNutritionalScore(energy, saturatedFat, sugars, salt) -
                getPositiveNutritionalScore(fiber, proteins);

    }
}

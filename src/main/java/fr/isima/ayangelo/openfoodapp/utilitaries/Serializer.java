package fr.isima.ayangelo.openfoodapp.utilitaries;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import fr.isima.ayangelo.openfoodapp.dto.Basket;

public class Serializer {
    private static final Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();

    public static JsonObject deserialize(String nutritionalInfo) {
        return gson.fromJson(nutritionalInfo, JsonObject.class);
    }

    public static String serialize(Basket basket) {
        return gson.toJson(basket);
    }
}

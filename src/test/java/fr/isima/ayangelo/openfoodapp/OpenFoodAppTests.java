package fr.isima.ayangelo.openfoodapp;

import fr.isima.ayangelo.openfoodapp.dto.Basket;
import fr.isima.ayangelo.openfoodapp.utilitaries.Serializer;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
class OpenFoodAppTests {

	@Autowired
	private MockMvc mvc;

	/**
	 GET : product/{barCode} tests
	 */

	@Test
	public void getProductPrinceChocolat() throws Exception {

		mvc.perform(MockMvcRequestBuilders.get("/product/7622210449283")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.score",
						Matchers.is(10)))
				.andExpect(MockMvcResultMatchers.jsonPath("$.name",
						Matchers.is("Prince Chocolat")))
				.andExpect(MockMvcResultMatchers.jsonPath("$.classification",
						Matchers.is(FoodClassification.MANGEABLE.getClassification())))
				.andExpect(MockMvcResultMatchers.jsonPath("$.color",
						Matchers.is(FoodClassification.MANGEABLE.getColor())))
				.andExpect(MockMvcResultMatchers.jsonPath("$.barCode",
						Matchers.is("7622210449283")));
	}

	@Test
	public void getProductLightCoke() throws Exception {

		mvc.perform(MockMvcRequestBuilders.get("/product/8888002076009")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.score",
						Matchers.is(2)))
				.andExpect(MockMvcResultMatchers.jsonPath("$.name",
						Matchers.is("Coke")))
				.andExpect(MockMvcResultMatchers.jsonPath("$.classification",
						Matchers.is(FoodClassification.BON.getClassification())))
				.andExpect(MockMvcResultMatchers.jsonPath("$.color",
						Matchers.is(FoodClassification.BON.getColor())))
				.andExpect(MockMvcResultMatchers.jsonPath("$.barCode",
						Matchers.is("8888002076009")));
	}

	@Test
	public void getProductNotFound() throws Exception {

		mvc.perform(MockMvcRequestBuilders.get("/product/notfoundtest")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isNotFound());

	}

	/**
		GET : /basket tests
	 */

	@Test
	public void getNutritionalScoreEmptyBasket() throws Exception {
		mvc.perform(MockMvcRequestBuilders.post("/basket")
				.contentType(MediaType.APPLICATION_JSON)
				.content(Serializer.serialize(new Basket("aymeric.angelo@test.com", List.of("")))))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().string("0"));
	}

	@Test
	public void getNutritionalScoreInvalidEmail() throws Exception {
		mvc.perform(MockMvcRequestBuilders.post("/basket")
				.contentType(MediaType.APPLICATION_JSON)
				.content(Serializer.serialize(new Basket("invalidEmail", List.of("7622210449283")))))
				.andExpect(MockMvcResultMatchers.status().is(400));
	}

	@Test
	public void getNutritionalScoreBasketOneItem() throws Exception {
		mvc.perform(MockMvcRequestBuilders.post("/basket")
				.contentType(MediaType.APPLICATION_JSON)
				.content(Serializer.serialize(new Basket("aymeric.angelo@test.com", List.of("7622210449283")))))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().string("10"));
	}

	@Test
	public void getNutritionalScoreBasketMultipleItemsOneInvalid() throws Exception {
		this.mvc.perform(MockMvcRequestBuilders.post("/basket")
				.contentType(MediaType.APPLICATION_JSON)
				.content(Serializer.serialize(new Basket("aymeric.angelo@test.com",
								List.of("7622210449283","notfound")))))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().string("10"));
	}

	@Test
	public void getNutritionalScoreBasketMultipleSameValidItems() throws Exception {
		this.mvc.perform(MockMvcRequestBuilders.post("/basket")
				.contentType(MediaType.APPLICATION_JSON)
				.content(Serializer.serialize(new Basket("aymeric.angelo@test.com",
								List.of("7622210449283","7622210449283","7622210449283")))))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().string("30"));
	}

	@Test
	public void getNutritionalScoreBasketMultipleDifferentValidItems() throws Exception {
		this.mvc.perform(MockMvcRequestBuilders.post("/basket")
				.contentType(MediaType.APPLICATION_JSON)
				.content(Serializer.serialize(new Basket("aymeric.angelo@test.com",
								List.of("7622210449283","8888002076009","7622210449283")))))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().string("22"));
	}

}

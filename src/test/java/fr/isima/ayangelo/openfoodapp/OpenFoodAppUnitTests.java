package fr.isima.ayangelo.openfoodapp;

import fr.isima.ayangelo.openfoodapp.utilitaries.NutritionalScoreComputer;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class OpenFoodAppUnitTests {
    @Test
    public void getFoodClassification() throws Exception {
        Assertions.assertEquals(FoodClassification.fromScore(-1), FoodClassification.TROP_BON);
        Assertions.assertEquals(FoodClassification.fromScore(-50), FoodClassification.TROP_BON);

        Assertions.assertEquals(FoodClassification.fromScore(0), FoodClassification.BON);
        Assertions.assertEquals(FoodClassification.fromScore(1), FoodClassification.BON);

        Assertions.assertEquals(FoodClassification.fromScore(3), FoodClassification.MANGEABLE);
        Assertions.assertEquals(FoodClassification.fromScore(8), FoodClassification.MANGEABLE);

        Assertions.assertEquals(FoodClassification.fromScore(11), FoodClassification.MOUAIS);
        Assertions.assertEquals(FoodClassification.fromScore(15), FoodClassification.MOUAIS);

        Assertions.assertEquals(FoodClassification.fromScore(19), FoodClassification.DEGUEU);
        Assertions.assertEquals(FoodClassification.fromScore(90), FoodClassification.DEGUEU);
    }

    @Test
    public void getEnergyNutritionalScore() throws Exception {
        Assertions.assertEquals(NutritionalScoreComputer.getEnergyNutritionalScore(-50), 0);
        Assertions.assertEquals(NutritionalScoreComputer.getEnergyNutritionalScore(335), 0);

        Assertions.assertEquals(NutritionalScoreComputer.getEnergyNutritionalScore(500), 1);

        Assertions.assertEquals(NutritionalScoreComputer.getEnergyNutritionalScore(700), 2);

        Assertions.assertEquals(NutritionalScoreComputer.getEnergyNutritionalScore(1100), 3);

        Assertions.assertEquals(NutritionalScoreComputer.getEnergyNutritionalScore(1675), 4);

        Assertions.assertEquals(NutritionalScoreComputer.getEnergyNutritionalScore(1676), 5);

        Assertions.assertEquals(NutritionalScoreComputer.getEnergyNutritionalScore(2100), 6);

        Assertions.assertEquals(NutritionalScoreComputer.getEnergyNutritionalScore(2400), 7);

        Assertions.assertEquals(NutritionalScoreComputer.getEnergyNutritionalScore(2700), 8);

        Assertions.assertEquals(NutritionalScoreComputer.getEnergyNutritionalScore(3100), 9);

        Assertions.assertEquals(NutritionalScoreComputer.getEnergyNutritionalScore(5000), 10);
    }

    @Test
    public void getFiberNutritionalScore() throws Exception {
        Assertions.assertEquals(NutritionalScoreComputer.getFiberNutritionalScore(-666),0);
        Assertions.assertEquals(NutritionalScoreComputer.getFiberNutritionalScore(0.9),0);

        Assertions.assertEquals(NutritionalScoreComputer.getFiberNutritionalScore(1),1);

        Assertions.assertEquals(NutritionalScoreComputer.getFiberNutritionalScore(2),2);

        Assertions.assertEquals(NutritionalScoreComputer.getFiberNutritionalScore(2.9),3);

        Assertions.assertEquals(NutritionalScoreComputer.getFiberNutritionalScore(4),4);

        Assertions.assertEquals(NutritionalScoreComputer.getFiberNutritionalScore(10),5);
    }

    @Test
    public void getProteinNutritionalScore() throws Exception {
        Assertions.assertEquals(NutritionalScoreComputer.getProteinNutritionalScore(-666), 0);
        Assertions.assertEquals(NutritionalScoreComputer.getProteinNutritionalScore(1.6), 0);

        Assertions.assertEquals(NutritionalScoreComputer.getProteinNutritionalScore(2), 1);

        Assertions.assertEquals(NutritionalScoreComputer.getProteinNutritionalScore(3.3), 2);

        Assertions.assertEquals(NutritionalScoreComputer.getProteinNutritionalScore(5), 3);

        Assertions.assertEquals(NutritionalScoreComputer.getProteinNutritionalScore(8), 4);

        Assertions.assertEquals(NutritionalScoreComputer.getProteinNutritionalScore(666), 5);
    }

    @Test
    public void getSaltNutritionalScore() throws Exception {
        Assertions.assertEquals(NutritionalScoreComputer.getSaltNutritionalScore(-666), 0);
        Assertions.assertEquals(NutritionalScoreComputer.getSaltNutritionalScore(90), 0);

        Assertions.assertEquals(NutritionalScoreComputer.getSaltNutritionalScore(180), 1);

        Assertions.assertEquals(NutritionalScoreComputer.getSaltNutritionalScore(270), 2);

        Assertions.assertEquals(NutritionalScoreComputer.getSaltNutritionalScore(360), 3);

        Assertions.assertEquals(NutritionalScoreComputer.getSaltNutritionalScore(450), 4);

        Assertions.assertEquals(NutritionalScoreComputer.getSaltNutritionalScore(540), 5);

        Assertions.assertEquals(NutritionalScoreComputer.getSaltNutritionalScore(630), 6);

        Assertions.assertEquals(NutritionalScoreComputer.getSaltNutritionalScore(720), 7);

        Assertions.assertEquals(NutritionalScoreComputer.getSaltNutritionalScore(810), 8);

        Assertions.assertEquals(NutritionalScoreComputer.getSaltNutritionalScore(900), 9);

        Assertions.assertEquals(NutritionalScoreComputer.getSaltNutritionalScore(999), 10);
    }

    @Test
    public void getSaturatedFatsNutritionalScore() throws Exception {
        Assertions.assertEquals(NutritionalScoreComputer.getSaturatedFatNutritionalScore(-666), 0);
        Assertions.assertEquals(NutritionalScoreComputer.getSaturatedFatNutritionalScore(1), 0);

        Assertions.assertEquals(NutritionalScoreComputer.getSaturatedFatNutritionalScore(2), 1);

        Assertions.assertEquals(NutritionalScoreComputer.getSaturatedFatNutritionalScore(3), 2);

        Assertions.assertEquals(NutritionalScoreComputer.getSaturatedFatNutritionalScore(4), 3);

        Assertions.assertEquals(NutritionalScoreComputer.getSaturatedFatNutritionalScore(5), 4);

        Assertions.assertEquals(NutritionalScoreComputer.getSaturatedFatNutritionalScore(6), 5);

        Assertions.assertEquals(NutritionalScoreComputer.getSaturatedFatNutritionalScore(7), 6);

        Assertions.assertEquals(NutritionalScoreComputer.getSaturatedFatNutritionalScore(8), 7);

        Assertions.assertEquals(NutritionalScoreComputer.getSaturatedFatNutritionalScore(9), 8);

        Assertions.assertEquals(NutritionalScoreComputer.getSaturatedFatNutritionalScore(10), 9);

        Assertions.assertEquals(NutritionalScoreComputer.getSaturatedFatNutritionalScore(666), 10);
    }

    @Test
    public void getSugarsNutritionalScore() throws Exception {
        Assertions.assertEquals(NutritionalScoreComputer.getSugarsNutritionalScore(-666), 0);
        Assertions.assertEquals(NutritionalScoreComputer.getSugarsNutritionalScore(4.5), 0);

        Assertions.assertEquals(NutritionalScoreComputer.getSugarsNutritionalScore(9), 1);

        Assertions.assertEquals(NutritionalScoreComputer.getSugarsNutritionalScore(13.5), 2);

        Assertions.assertEquals(NutritionalScoreComputer.getSugarsNutritionalScore(18), 3);

        Assertions.assertEquals(NutritionalScoreComputer.getSugarsNutritionalScore(22.5), 4);

        Assertions.assertEquals(NutritionalScoreComputer.getSugarsNutritionalScore(27), 5);

        Assertions.assertEquals(NutritionalScoreComputer.getSugarsNutritionalScore(31), 6);

        Assertions.assertEquals(NutritionalScoreComputer.getSugarsNutritionalScore(36), 7);

        Assertions.assertEquals(NutritionalScoreComputer.getSugarsNutritionalScore(40), 8);

        Assertions.assertEquals(NutritionalScoreComputer.getSugarsNutritionalScore(45), 9);

        Assertions.assertEquals(NutritionalScoreComputer.getSugarsNutritionalScore(999), 10);
    }

}
